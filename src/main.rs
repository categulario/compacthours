use csvsc::{
    InputStream, ReaderSource, ColSpec, RowStream, aggregate::{
        Aggregate, AggregateError,
    }, Headers, Row, FlushTarget,
};
use encoding::all::UTF_8;
use regex::Regex;
use time::Time;
use std::path::PathBuf;

#[derive(Debug)]
struct TimeAggregate {
    seconds: f64
}

impl TimeAggregate {
    fn new() -> TimeAggregate {
        TimeAggregate {
            seconds: 0.0,
        }
    }
}

impl Aggregate for TimeAggregate {
    fn update(&mut self, headers: &Headers, row: &Row) -> Result<(), AggregateError> {
        let start_time = Time::parse(headers.get_field(row, "start_time").unwrap(), "%T").unwrap();
        let end_time = Time::parse(headers.get_field(row, "end_time").unwrap(), "%T").unwrap();

        self.seconds += (end_time - start_time).as_seconds_f64();

        Ok(())
    }

    fn value(&self) -> String {
        (self.seconds / 3600.0).to_string()
    }

    fn colname(&self) -> &str {
        "hours"
    }
}

fn main() {
    let mut chain = InputStream::new(
            ReaderSource::from_path("/dev/stdin").unwrap(),
            UTF_8,
        )
        .add(ColSpec::Regex {
            source: "start".into(),
            colname: "date".into(),
            coldef: "$1".to_string(),
            regex: Regex::new(r"(\d{4}-\d{2}-\d{2})").unwrap(),
        }).unwrap()
        .add(ColSpec::Regex {
            source: "start".into(),
            colname: "start_time".into(),
            coldef: "$1".to_string(),
            regex: Regex::new(r"(\d{2}:\d{2}:\d{2})").unwrap(),
        }).unwrap()
        .add(ColSpec::Regex {
            source: "end".into(),
            colname: "end_time".into(),
            coldef: "$1".to_string(),
            regex: Regex::new(r"(\d{2}:\d{2}:\d{2})").unwrap(),
        }).unwrap()
        .adjacent_group(|mut headers| {
            headers.add("hours").unwrap();

            headers
        }, |row_stream| {
            row_stream
                .reduce(vec![
                    Box::new(TimeAggregate::new()),
                ]).unwrap()
        }, &["date"]).unwrap()
        .del(vec!["start", "end", "note", "sheet", "start_time", "end_time"])
        .flush(FlushTarget::Path(PathBuf::from("/dev/stdout"))).unwrap()
        .into_iter();

    while let Some(item) = chain.next() {
        if let Err(e) = item {
            println!("{}", e);
        }
    }
}
