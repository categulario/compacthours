# compacthours

A simple program that takes the output from timetrap:

`t d -g <something> -f csv`

and outputs a summary of the hours worked by date. I use it like this:

`t d -g <client> -f csv | compacthours > hours.csv`
